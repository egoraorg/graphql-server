# graphql-server

The GraphQL server that proxies all Egora APIs.

## Devlopment

Set the Access Token to your user access token.

token: 'Bearer {{access_token}}'

Read how to get the access token here:

https://t2bot.io/docs/access_tokens/

`node index.js` to run

The playground is accessible at http://localhost:4000/

You can query for users or set admin with these graphql queries

query {
  users {
    name
    admin
  }
}

query {
  user(name: "@username:egora.org") {
    name
    admin
  }
}

mutation {
  login(name: "@username:egora.org", password: "password") {
  {
    access_token
    home_server
    user_id
  }
}

mutation {
  setUserAdmin(name:"@username:egora.org",admin:false)
  {
    name
    admin
  }
}

mutation {
  setUserPowerLevel(roomId:"!THXguMsFBWbbXEyimX:egora.org",name:"@username:egora.org",powerLevel:0) {
    event_id
  }
}

mutation {
  deactivateUser(name:"@test_deactivate_user:egora.org")
}

mutation {
  deleteRoom(name:"!THXguMsFBWbbXEyimX:egora.org")
}
