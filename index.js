const { ApolloServer, gql } = require('apollo-server');
const { RESTDataSource } = require('apollo-datasource-rest');

const typeDefs = gql`
  "Login"
  type Login {
    """
    The access token
    """
    access_token: String
    """
    The server the token belongs to
    """
    home_server: String
    """
    The username the token is for
    """
    user_id: String
  }

  "User"
  type User {
    """
    Username
    """
    name: String
    """
    Is the User a server admin?
    """
    admin: Boolean
    """
    Is the User deactivated?
    """
    deactivated: Boolean
  }

  "Event"
  type Event {
    """
    Event ID
    """
    event_id: String
  }

  "DeletedRoom"
  type DeletedRoom {
    """
    The users who were in the room who got kicked.
    """
    kicked_users: [String]
    """
    The users who could not be kicked from the room.
    """
    failed_to_kick_users: [String]
  }

  type Query {
    user(name: String): User
    users: [User]
  }

  type Mutation {
    login(name: String, password: String): Login
    setUserAdmin(name: String, admin: Boolean): User
    setUserPowerLevel(roomId: String, name: String, powerLevel: Int): Event
    deactivateUser(name: String): Boolean
    kickUser(roomId: String, name: String, reason: String): Boolean
    banUser(roomId: String, name: String, reason: String): Boolean
    unbanUser(roomId: String, name: String): Boolean
    deleteRoom(roomId: String): DeletedRoom
    deleteCommunity(groupId: String): Boolean
  }
`;

const resolvers = {
  Query: {
    user: async (_source, { name }, { dataSources }) => {
      return dataSources.synapseAPI.getUser(name);
    },
    users: async (_source, _args, { dataSources }) => {
      return dataSources.synapseAPI.getUsers();
    },
  },
  Mutation: {
    login: async (_source, { name, password }, { dataSources }) => {
      return dataSources.synapseAPI.login(name, password);
    },
    setUserAdmin: async (_, { name, admin }, { dataSources }) => {
      return dataSources.synapseAPI.setUserAdmin(name, admin);
    },
    setUserPowerLevel: async (_, { roomId, name, powerLevel }, { dataSources }) => {
      return dataSources.synapseAPI.setUserPowerLevel(roomId, name, powerLevel);
    },
    deactivateUser: async (_, { name }, { dataSources }) => {
      return dataSources.synapseAPI.deactivateUser(name).then(d => d.id_server_unbind_result === 'success');
    },
    kickUser: async (_, { roomId, name, reason }, { dataSources }) => {
      return dataSources.synapseAPI.kickUser(roomId, name, reason);
    },
    banUser: async (_, { roomId, name, reason }, { dataSources }) => {
      return dataSources.synapseAPI.banUser(roomId, name, reason);
    },
    unbanUser: async (_, { roomId, name }, { dataSources }) => {
      return dataSources.synapseAPI.unbanUser(roomId, name);
    },
    deleteRoom: async (_, { roomId }, { dataSources }) => {
      return dataSources.synapseAPI.deleteRoom(roomId);
    },
    deleteCommunity: async (_, { groupId }, { dataSources }) => {
      return dataSources.synapseAPI.deleteCommunity(groupId);
    },
  },
};

class SynapseAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = 'https://matrix.egora.org:8448/';
  }

  willSendRequest(request) {
    request.headers.set('Authorization', this.context.token);
  }

  async login(name, password) {
    return this.post(`_matrix/client/r0/login`, { type: 'm.login.password', user: name, password: password });
  }

  async getUser(name) {
    return this.get(`_synapse/admin/v2/users/${name}`);
  }

  async getUsers(limit = 10) {
    const data = await this.get('_synapse/admin/v2/users', {
      from: 0,
      limit: 1000,
      guests: false,
    });
    return data.users;
  }

  async setUserAdmin(name, admin) {
    return this.put(`_synapse/admin/v2/users/${name}`, { admin });
  }

  async setUserPowerLevel(roomId, name, powerLevel) {
    return this.get(`_matrix/client/r0/rooms/${roomId}/state/m.room.power_levels`)
      .then(r => { r.users[name] = powerLevel; return r.users; })
      .then(users => this.put(`_matrix/client/r0/rooms/${roomId}/state/m.room.power_levels`, { users })); 
  }

  async deactivateUser(name) {
    return this.post(`_matrix/client/r0/admin/deactivate/${name}`);
  }

  async kickUser(roomId, name, reason) {
    return this.post(`_matrix/client/r0/rooms/${roomId}/kick`, { user_id: name, reason })
  }

  async banUser(roomId, name, reason) {
    return this.post(`_matrix/client/r0/rooms/${roomId}/ban`, { user_id: name, reason })
  }

  async unbanUser(roomId, name) {
    return this.post(`_matrix/client/r0/rooms/${roomId}/unban`, { user_id: name })
  }

  async deleteRoom(roomId) {
    return this.post(`_synapse/admin/v1/rooms/${roomId}/delete`, {});
  }

  async deleteCommunity(groupId) {
    return this.post(`_synapse/admin/v1/delete_group/${groupId}`).then(r => !r.errors || r.errors.length === 0);
  }
}

const server = new ApolloServer({ 
  typeDefs,
  resolvers,
  dataSources: () => {
    return {
      synapseAPI: new SynapseAPI(),
    };
  },
  context: () => {
    return {
      token: 'Bearer access_token',
    };
  },
});

server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
