FROM node:12
WORKDIR /usr/src/app
COPY package*.json ./
RUN curl -o- -L https://yarnpkg.com/install.sh | bash -s -- --version 1.22.5
RUN yarn install
COPY . .
EXPOSE 4000
CMD [ "node", "index.js" ]
